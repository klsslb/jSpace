package org.jspace.protocol;

public enum SpaceType {
    PILE,
    QUEUE,
    RANDOM,
    SEQUENTIAL,
    STACK,
}
