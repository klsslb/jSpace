package org.jspace.protocol;

public enum KeyType {
    PUT,
    GET,
    QUERY,
}
